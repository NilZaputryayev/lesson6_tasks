﻿using Common.DTO.Project;
using Common.DTO.Team;
using Common.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Test.Integrational;
using Xunit;

namespace WebAPI.Tests.Integrational
{
    public class TeamControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        public TeamControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateDefaultClient();
        }

        [Fact]
        public async Task CreateAsync_WhenCreateTeam_ReturnTeamDTO()
        {
            var team = new NewTeamDTO()
            {
                id = 9999,
                name = "Test Team",

            };

            var json = JsonSerializer.Serialize(team);

            var httpResponse = await _client.PostAsync("team/", new StringContent(json, Encoding.UTF8, "application/json"));

            var created = JsonSerializer.Deserialize<TeamDTO>(await httpResponse.Content.ReadAsStringAsync());


            Assert.Equal(System.Net.HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(typeof(TeamDTO), created.GetType());
            Assert.Equal("Test Team", created.name);

        }

        [Fact]
        public async Task CreateAsync_WhenCreateTeam_TeamCountIncreasedOnOne()
        {
            var team = new NewTeamDTO()
            {
                id = 9999,
                name = "Test Team",

            };

            var json = JsonSerializer.Serialize(team);

            var teamsBefore = await GetCount();

            var httpResponse = await _client.PostAsync("team", new StringContent(json, Encoding.UTF8, "application/json"));

            var teamsAfter = await GetCount();

            Assert.Equal(1, teamsAfter - teamsBefore);

        }

        public async Task<int> GetCount()
        {
            var httpResponse = await _client.GetAsync($"team/");

            return (JsonSerializer.Deserialize<List<TeamDTO>>(await httpResponse.Content.ReadAsStringAsync()).Count());

        }


        public void Dispose()
        {
        }
    }
}
