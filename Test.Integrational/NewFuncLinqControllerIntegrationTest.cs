﻿using Common.DTO;
using Common.DTO.Project;
using Common.DTO.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Test.Integrational;
using Xunit;

namespace WebAPI.Tests.Integrational
{
    public class NewFuncLinqControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        public NewFuncLinqControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateDefaultClient();
        }



        [Theory]
        [InlineData(35, 4)]
        [InlineData(28, 3)]
        public async Task GetProjectInfo_WhenTestedID_ThenReturnExpectedNumberOfTasks(int id, int expectedCount)
        {

            var httpResponse = await _client.GetAsync($"linq/GetUserUnfinishedTasks/{id}");

            var tasks = JsonConvert.DeserializeObject<List<Task>>(await httpResponse.Content.ReadAsStringAsync());

            var tasksCount = tasks.Count();

            Assert.Equal(System.Net.HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(expectedCount, tasksCount);
        }



        public void Dispose()
        {
        }
    }
}
