﻿using Common.DTO.Project;
using Common.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Test.Integrational;
using Xunit;

namespace WebAPI.Tests.Integrational
{
    public class TaskControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        public TaskControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateDefaultClient();
        }

        public async Task<int> GetCount()
        {
            var httpResponse = await _client.GetAsync($"task/");

            return (JsonSerializer.Deserialize<List<UserDTO>>(await httpResponse.Content.ReadAsStringAsync()).Count());

        }

        [Fact]
        public async Task DeleteAsync_WhenDeleteTask_TaskActualyDeleted()
        {
            var userID = 1;

            var userCountBefore = await GetCount();

            var httpResponse = await _client.DeleteAsync($"task/{userID}");

            var usercountAfter = await GetCount();

            Assert.Equal(System.Net.HttpStatusCode.NoContent, httpResponse.StatusCode);
            Assert.Equal(1, userCountBefore - usercountAfter);

        }

        [Fact]
        public async Task DeleteAsync_WhenDeleteTaskIDNotFound_ReturnNotFound()
        {
            var id = 99999;

            var countBefore = await GetCount();

            var httpResponse = await _client.DeleteAsync($"task/{id}");

            var countAfter = await GetCount();

            Assert.Equal(System.Net.HttpStatusCode.NotFound, httpResponse.StatusCode);
            Assert.Equal(0, countBefore - countAfter);

        }

        public void Dispose()
        {
        }
    }
}
