﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient
{
    public abstract class BaseService
    {
        private static readonly HttpClient client = new HttpClient();

        readonly string BaseUrl = "https://localhost:5001/api/";
        public virtual string Endpoint { get; set; }

        private static JsonSerializerSettings jsonSettings;

        protected BaseService()
        {
            ConfigureClient();
        }

        public void ConfigureClient()
        {
            client.BaseAddress = new Uri(BaseUrl);
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");

            jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public virtual async Task<ICollection<T>> Get<T>()
        {
            List<T> data = new List<T>();

            string endpoint = Endpoint;

            try
            {
                var response = await client.GetAsync(endpoint);

                if (response.IsSuccessStatusCode)


                    data = JsonConvert.DeserializeObject<List<T>>(await response.Content.ReadAsStringAsync(), jsonSettings);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return data;


        }

        public virtual async Task<T> GetOne<T>()
        {
            T data = default(T);

            string endpoint = Endpoint;

            try
            {
                var response = await client.GetAsync(endpoint);

                if (response.IsSuccessStatusCode)


                    data = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync(), jsonSettings);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return data;


        }

        public virtual async Task Put<T>(int id,T entity)
        {
            string endpoint = Endpoint;

            string json = JsonConvert.SerializeObject(entity);

            try
            {
                var response = await client.PutAsync($"{endpoint}/{id}", new StringContent(json, Encoding.UTF8, "application/json"));

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }



        }

    }
}
