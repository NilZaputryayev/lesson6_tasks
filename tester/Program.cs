﻿using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace ConsoleClient
{
    class Program
    {

        static readonly LinqService linqService = new LinqService();
        static readonly TaskService taskService = new TaskService();
        static Random random = new Random();

        static async Task Main(string[] args)
        {


            bool showMenu = true;
            while (showMenu)
            {
                showMenu = await MainMenuAsync();
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }

        }
        public static async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var tcs = new TaskCompletionSource<int>();

            Timer timer = new Timer(delay);

            timer.Elapsed += async (sender, args) =>
            {
                timer.Enabled = false;

                try
                {
                    PrintToConsole($"Timer stoped: {DateTime.Now}", ConsoleColor.Magenta);
                    var result = await WorkWithTask();
                    tcs.SetResult(result);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }

            };

            timer.Enabled = true;
            PrintToConsole($"Timer started: {DateTime.Now}", ConsoleColor.Magenta);


            return await tcs.Task;
        }
        private static async Task<int> WorkWithTask()
        {
           // throw new Exception("Ups big problem, realy big");

            var tasks = (await taskService.Get<TaskDTO>())
                                .Where(x => x.finishedAt == DateTime.MinValue);

            var randomTask = tasks
                                .Skip(random.Next(tasks.Count() - 1)).FirstOrDefault();

            randomTask.finishedAt = DateTime.Now;

            await taskService.Put(randomTask.id, randomTask);

            return randomTask.id;
        }


        private static void PrintToConsole(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private static async Task<bool> MainMenuAsync()
        {

            Console.Clear();

            Console.WriteLine("Select an option:");
            Console.WriteLine("1. Get count of tasks from user (id)");
            Console.WriteLine("2. Get list of tasks where name < 45 (id)");
            Console.WriteLine("3. List of finished in 2021 year tasks (id)");
            Console.WriteLine("4. List (id, name, users) from teams where users age > 10");
            Console.WriteLine("5. List of users sorted by first_name & task sorted by name desc");
            Console.WriteLine("6. Get UserInfo  (id)");
            Console.WriteLine("7. Get Project info (id)");
            Console.WriteLine("8. Start random task finisher");
            Console.WriteLine("0. Exit");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":

                    await Task1();
                    return true;
                case "2":
                    await Task2();
                    return true;
                case "3":
                    await Task3();
                    return true;
                case "4":
                    await Task4();
                    return true;
                case "5":
                    await Task5();
                    return true;
                case "6":
                    await Task6();
                    return true;
                case "7":
                    await Task7();
                    return true;
                case "8":
                    Task8();
                    return true;
                case "0":
                    return false;
                default:
                    return true;
            }
        }

        private static async Task Task8()
        {
            PrintToConsole($"Task finisher started at {DateTime.Now}", ConsoleColor.Green);

            try
            {
                int id = await MarkRandomTaskWithDelay(1000);

                PrintToConsole($"task with id:{id} was finished at {DateTime.Now}", ConsoleColor.Green);
            }
            catch (Exception ex)
            {
                PrintToConsole($"exception happend {ex.Message}", ConsoleColor.Red);
            }

        }

        private static async Task Task1()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
            {
                linqService.Endpoint = $"Linq/GetUserTaskCountDictionaryByID/{id}";
                try
                {
                    Printer.Print(await linqService.Get<dynamic>());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        private static async Task Task2()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
            {
                linqService.Endpoint = $"Linq/ListTasksWith45NameByID/{id}";
                Printer.Print((await linqService.Get<TaskDTO>()).ToList());
            }
        }
        private static async Task Task3()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
                linqService.Endpoint = $"Linq/TaskListFinished2021ByID/{id}";
            Printer.Print((await linqService.Get<TaskIdName>()).ToList());

        }
        private static async Task Task4()
        {
            linqService.Endpoint = $"Linq/GetTeamsOlder10SortedAndGrouped";
            Printer.Print((await linqService.Get<TeamInfoDTO>()).ToList());

        }
        private static async Task Task5()
        {
            linqService.Endpoint = $"Linq/GetUserListSortedWithTasks";
            Printer.Print(await linqService.GetOne<Dictionary<string, List<TaskDTO>>>());
        }
        private static async Task Task6()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
            {
                linqService.Endpoint = $"Linq/GetUserTasksInfo/{id}";
                (await linqService.GetOne<UserTasksInfoDTO>())?.Print();
            }
        }
        private static async Task Task7()
        {
            if (Int32.TryParse(GetStringFromConsole("projectID"), out int id))
            {
                linqService.Endpoint = $"Linq/GetProjectInfo/{id}";
                (await linqService.GetOne<ProjectInfoDTO>())?.Print();
            }

        }



        private static string GetStringFromConsole(string message)
        {
            string result;
            do
            {
                Console.WriteLine($"Enter {message}:");
                result = Console.ReadLine();

            } while (string.IsNullOrEmpty(result));

            return result;
        }



    }
}
