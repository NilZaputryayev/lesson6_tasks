﻿
using Common.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO
{
    public class TeamInfoDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public IEnumerable<UserDTO> users { get; set; }

    }
}
