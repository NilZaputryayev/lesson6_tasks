﻿using System;

namespace Common.DTO.Team
{
    public class NewTeamDTO
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}