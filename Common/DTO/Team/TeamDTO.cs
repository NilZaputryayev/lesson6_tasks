﻿using Common.DTO.User;
using System;
using System.Collections.Generic;


namespace Common.DTO.Team
{
    public class TeamDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createdAt { get; set; }
    }
}