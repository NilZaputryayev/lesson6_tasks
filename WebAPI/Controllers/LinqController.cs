﻿using BLL;
using Common.DTO.Project;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class LinqController : ControllerBase
    {
        private readonly IApiLinqService _LinqService;

        public LinqController(IApiLinqService LinqService)
        {
            _LinqService = LinqService;
        }

        [HttpGet]
        [Route("GetProjectInfo/{id}")]
        public async Task<ActionResult> GetProjectInfo(int id)
        {
            var res = await _LinqService.GetProjectInfo(id);
            return Ok(res);
           // return Ok(_LinqService.GetProjectInfo(id));
        }

        [HttpGet]
        [Route("GetUserTasksInfo/{id}")]
        public async Task<ActionResult> GetUserTasksInfo(int id)
        {
            return Ok(await _LinqService.GetUserTasksInfo(id));
        }

        [HttpGet]
        [Route("GetUserListSortedWithTasks")]
        public async  Task<ActionResult> GetGetUserListSortedWithTask()
        {
            return Ok(await _LinqService.GetUserListSortedWithTasksAsync());
        }

        [HttpGet]
        [Route("GetTeamsOlder10SortedAndGrouped")]
        public async Task<ActionResult> GetTeamsOlder10SortedAndGrouped()
        {
            return Ok(await _LinqService.GetTeamsOlder10SortedAndGrouped());
        }
         
        [HttpGet]
        [Route("ListTasksWith45NameByID/{id}")]
        public async Task<ActionResult> ListTasksWith45NameByID(int id)
        {
            return Ok(await _LinqService.ListTasksWith45NameByID(id));
        }

        [HttpGet]
        [Route("TaskListFinished2021ByID/{id}")]
        public async Task<ActionResult> TaskListFinished2021ByID(int id)
        {
            return Ok(await _LinqService.TaskListFinished2021ByID(id));
        }

        [HttpGet]
        [Route("GetUserTaskCountDictionaryByID/{id}")]
        public async Task<ActionResult> GetUserTaskCountDictionaryByID(int id)
        {
            return Ok((await _LinqService.GetUserTaskCountDictionaryByID(id)).ToList());
        }

        [HttpGet]
        [Route("GetUserUnfinishedTasks/{id}")]
        public async Task<ActionResult> GetUserUnfinishedTasks(int id)
        {
            var test = (await _LinqService.GetUserUnfinishedUserTasksByUserID(id)).ToList();
            return Ok(test);
//            return Ok(_LinqService.GetUserUnfinishedUserTasksByUserID(id).ToList());
        }

    }
}
