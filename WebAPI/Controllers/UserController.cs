﻿using BLL;
using Common.DTO.User;
using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class UserController : ControllerBase
    {
        private readonly IApiUserService _userService;

        public UserController(IApiUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var usr = await _userService.Get();
            return Ok(usr);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetAsync(int id)
        {
            var entity = await _userService.GetByID(id);

            if (entity == null) return NotFound(new { message = "not found" });

            return Ok(entity);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateUserDTO userDTO)
        {
            await _userService.Update(userDTO);
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> CreateAsync(NewUserDTO userDTO)
        {          

            return Ok(await _userService.Create(userDTO));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                await _userService.Delete(id);
            }
            catch(ArgumentException)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
