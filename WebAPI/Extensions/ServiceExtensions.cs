﻿using AutoMapper;
using BLL;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BLL.MappingProfiles;
using System.Collections.Generic;
using Task = DAL.Models.Task;
using DAL.Context;

namespace WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServicesAsync(this IServiceCollection services)
        {

            services.AddHttpClient("ExternalApiClient", client =>
             {
                 client.BaseAddress = new Uri("https://bsa21.azurewebsites.net/api/");
                 client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
             });

            services.AddHttpClient("InternalApiClient", client =>
            {
                client.BaseAddress = new Uri("https://localhost:5001/api/");
                client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            });

            services.AddTransient<IApiUserService, ApiUserService>();
            services.AddTransient<IApiTeamService, ApiTeamService>();
            services.AddTransient<IApiTaskService, ApiTaskService>();
            services.AddTransient<IApiProjectService, ApiProjectService>();
            services.AddTransient<IApiLinqService, ApiLinqService>();


        }



        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<ProjectInfoProfile>();
                cfg.AddProfile<UserTasksInfoProfile>();
                cfg.AddProfile<TeamInfoProfile>();
            },
            Assembly.GetExecutingAssembly());
        }



    }
}
