﻿using AutoMapper;
using Common.DTO.Task;
using DAL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BLL
{
    public class ApiTaskService : BaseService, IApiTaskService
    {

        public ApiTaskService(LinqEFContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<List<TaskDTO>> Get()
        {
            return _mapper.Map<List<TaskDTO>>(await _context.Tasks.ToListAsync());
        }
        public async Task<TaskDTO> GetByID(int id)
        {
            return _mapper.Map<TaskDTO>(await _context.Tasks.FindAsync(id));
        }
        public async Task Update(UpdateTaskDTO DTO)
        {
            var usr = await _context.Tasks.FindAsync(DTO.id);

            if (usr != null)
            {
                usr.name = DTO.name;
                usr.description = DTO.description;
                usr.finishedAt = DTO.finishedAt;
                usr.performerId = DTO.performerId;

                await _context.SaveChangesAsync();
            }
        }
        public async Task Delete(int id)
        {
            var entity = await _context.Tasks.FindAsync(id);

            if (entity == null) throw new ArgumentException();

            _context.Tasks.Remove(entity);

            await _context.SaveChangesAsync();
        }

        public async Task<TaskDTO> Create(NewTaskDTO entity)
        {
            DAL.Models.Task model = _mapper.Map<DAL.Models.Task>(entity);

            var created = await _context.Tasks.AddAsync(model);

            var createdDTO = _mapper.Map<TaskDTO>(created.Entity);

            await _context.SaveChangesAsync();

            return createdDTO;
        }

    }
}
