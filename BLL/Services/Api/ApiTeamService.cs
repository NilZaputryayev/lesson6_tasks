﻿using AutoMapper;
using Common.DTO.Team;
using DAL.Context;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BLL
{
    public class ApiTeamService : BaseService, IApiTeamService
    {

        public ApiTeamService(LinqEFContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<List<TeamDTO>> Get()
        {

            return _mapper.Map<List<TeamDTO>>(await _context.Teams.ToListAsync());
        }
        public async Task<TeamDTO> GetByID(int id)
        {
            return _mapper.Map<TeamDTO>(await _context.Teams.FindAsync(id));
        }
        public async Task Update(UpdateTeamDTO DTO)
        {
            var entity = await _context.Teams.FindAsync(DTO.id);

            if (entity != null)
            {
                entity.name = DTO.name;
                _context.Teams.Update(entity);

            }
        }
        public async Task Delete(int id)
        {
            var entity = await _context.Teams.FindAsync(id);
            _context.Teams.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<TeamDTO> Create(NewTeamDTO entity)
        {
            var model = _mapper.Map<Team>(entity);

            var created = await _context.Teams.AddAsync(model);

            var createdDTO = _mapper.Map<TeamDTO>(created.Entity);

            await _context.SaveChangesAsync();

            return createdDTO;

        }


    }
}
