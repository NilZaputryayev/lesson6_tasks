﻿using AutoMapper;
using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using DAL;
using DAL.Context;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BLL
{
    public class ApiLinqService : BaseService, IApiLinqService
    {
        private IEnumerable<Project> ProjectData;
        static IEnumerable<Team> TeamData;


        public ApiLinqService(LinqEFContext context, IMapper mapper) : base(context, mapper)
        {
            GenerateStructureAsync().Wait();
        }

        private async Task GenerateStructureAsync()
        {

            var tasks = await _context.Tasks.ToListAsync();
            var users = await _context.Users.ToListAsync();
            var teams = await _context.Teams.ToListAsync();
            var projects = await _context.Projects.ToListAsync();


            IEnumerable<DAL.Models.Task> taskPerformers = from t in tasks
                                               join u in users on t.performerId equals u.id into tp
                                               from task_performer in tp
                                               select new DAL.Models.Task
                                               {
                                                   id = t.id,
                                                   createdAt = t.createdAt,
                                                   description = t.description,
                                                   finishedAt = t.finishedAt,
                                                   state = t.state,
                                                   name = t.name,
                                                   performerId = t.performerId,
                                                   projectId = t.projectId,
                                                   Performer = task_performer
                                               };

            teams = (from t in teams
                     join u in users on t.id equals u.teamId into team_users
                     select new Team
                     {
                         id = t.id,
                         createdAt = t.createdAt,
                         name = t.name,
                         users = team_users
                     }).ToList();



            ProjectData = from p in projects
                          join a in users on p.authorId equals a.id into pa
                          from project_author in pa
                          join m in teams on p.teamId equals m.id into pt
                          from project_team in pt
                          join t in taskPerformers on p.id equals t.projectId into project_tasks
                          select new Project
                          {
                              id = p.id,
                              authorId = p.authorId,
                              teamId = p.teamId,
                              name = p.name,
                              description = p.description,
                              createdAt = p.createdAt,
                              deadline = p.deadline,
                              Tasks = project_tasks,
                              Author = project_author,
                              Team = project_team
                          };

            TeamData = from t in teams
                       join u in users on t.id equals u.teamId into team_members
                       select new Team
                       {
                           createdAt = t.createdAt,
                           name = t.name,
                           id = t.id,
                           users = team_members
                       };
        }

        private void GenerateStructure()
        {

            var tasks = _context.Tasks.ToList();
            var users = _context.Users.ToList();
            var teams = _context.Teams.ToList();
            var projects = _context.Projects.ToList();


            IEnumerable<DAL.Models.Task> taskPerformers = from t in tasks
                                               join u in users on t.performerId equals u.id into tp
                                               from task_performer in tp
                                               select new DAL.Models.Task
                                               {
                                                   id = t.id,
                                                   createdAt = t.createdAt,
                                                   description = t.description,
                                                   finishedAt = t.finishedAt,
                                                   state = t.state,
                                                   name = t.name,
                                                   performerId = t.performerId,
                                                   projectId = t.projectId,
                                                   Performer = task_performer
                                               };

            teams = (from t in teams
                     join u in users on t.id equals u.teamId into team_users
                     select new Team
                     {
                         id = t.id,
                         createdAt = t.createdAt,
                         name = t.name,
                         users = team_users
                     }).ToList();



            ProjectData = from p in projects
                          join a in users on p.authorId equals a.id into pa
                          from project_author in pa
                          join m in teams on p.teamId equals m.id into pt
                          from project_team in pt
                          join t in taskPerformers on p.id equals t.projectId into project_tasks
                          select new Project
                          {
                              id = p.id,
                              authorId = p.authorId,
                              teamId = p.teamId,
                              name = p.name,
                              description = p.description,
                              createdAt = p.createdAt,
                              deadline = p.deadline,
                              Tasks = project_tasks,
                              Author = project_author,
                              Team = project_team
                          };

            TeamData = from t in teams
                       join u in users on t.id equals u.teamId into team_members
                       select new Team
                       {
                           createdAt = t.createdAt,
                           name = t.name,
                           id = t.id,
                           users = team_members
                       };
        }
        public async Task<Dictionary<ProjectDTO, int>> GetUserTaskCountDictionaryByID(int id)
        {
            return await Task.Run(() => 
                    ProjectData
                         .ToDictionary(key => _mapper.Map<ProjectDTO>(key), value => value.Tasks.Where(x => x.performerId == id).Count())
                         .Where(x => x.Value > 0)
                         .ToDictionary(x => x.Key, y => y.Value)
                         );
        }

        private async Task<Dictionary<ProjectDTO, int>> UserTaskInfoQuery(int id)
        {
            return await Task.Run(() =>
                     ProjectData
                         .ToDictionary(key => _mapper.Map<ProjectDTO>(key), value => value.Tasks.Where(x => x.performerId == id).Count())
                         .Where(x => x.Value > 0)
                         .ToDictionary(x => x.Key, y => y.Value)
                         );
        }

        public async Task<List<TaskDTO>> ListTasksWith45NameByID(int id)
        {
            return await Task.Run(() =>
            _mapper.Map<List<TaskDTO>>(
                ProjectData
                    .SelectMany(x => x.Tasks)
                    .Where(x => x.name.Length < 45 &&
                                x.performerId == id
                          )
                    .ToList()
                    ));
        }
        public async Task<List<TaskIdName>> TaskListFinished2021ByID(int id)
        {
            return await Task.Run(() =>
                    ProjectData
                    .SelectMany(x => x.Tasks)
                    .Where(x => x.finishedAt.Year == 2021 &&
                                x.performerId == id)
                    .Select(x => new TaskIdName { id = x.id, name = x.name })
                    .ToList());
        }
        public async Task<Dictionary<string, List<TaskDTO>>> GetUserListSortedWithTasksAsync()
        {
            return await Task.Run(() =>
                    ProjectData
                    .SelectMany(x => x.Tasks)
                    .Select(x => new
                    {
                        first_name = x.Performer.firstName,
                        task = _mapper.Map<TaskDTO>(x)
                    })
                    .OrderBy(x => x.first_name)
                    .ThenByDescending(x => x.task.name.Length)
                    .GroupBy(x => x.first_name, y => y.task)
                    .ToDictionary(x => x.Key, y => y.ToList())
                    );
        }

        public async Task<List<TeamInfoDTO>> GetTeamsOlder10SortedAndGrouped()
        {
            return await Task.Run(() =>
                    TeamData
                    .Where(x => x.users.All(u => DateTime.Now.Year - 10 > u.birthDay.Year))
                    .Select(x => _mapper.Map<TeamInfoDTO>(
                        new TeamInfo
                        {
                            id = x.id,
                            name = x.name,
                            users = x.users.OrderByDescending(x => x.registeredAt)
                        })
                    )
                    //.GroupBy(x => x.name)
                    .ToList()
                    );

        }
        public async Task<UserTasksInfoDTO> GetUserTasksInfo(int id)
        {
            return await Task.Run(() =>
            _mapper.Map<UserTasksInfoDTO>(
                ProjectData
                    .SelectMany(p => p.Tasks, (p, t) => new { Project = p, Task = t })
                    .GroupBy(x => x.Task.Performer)
                    .Where(x => x.Key.id == id)
                    .Select(x => new UserTasksInfo()
                    {
                        User = x.Key,

                        LastProject = x.OrderByDescending(x => x.Project.createdAt)
                                        .FirstOrDefault().Project,

                        LastProjectTasksCount = x.OrderByDescending(x => x.Project.createdAt)
                                                    .Select(x => x.Project.Tasks.Count())
                                                    .FirstOrDefault(),

                        CountCancelOrUnfinishedTasks = x.Where(x => x.Task.finishedAt == DateTime.MinValue)
                                                        .Count(),

                        LongestTask = x.Where(x => x.Task.finishedAt > DateTime.MinValue)
                                        .Select(x => new { task = x, taskLen = x.Task.finishedAt - x.Task.createdAt })
                                        .OrderByDescending(x => x.taskLen)
                                        .Select(x => x.task.Task)
                                        .FirstOrDefault()
                    }).FirstOrDefault()
                    )
            );
        }

        public async Task<List<TaskDTO>> GetUserUnfinishedUserTasksByUserID(int id)
        {
            return await Task.Run(() =>
            _mapper.Map<List<TaskDTO>>(
                ProjectData
                    .SelectMany(x => x.Tasks)
                    .Where(x => x.finishedAt == DateTime.MinValue &&
                                x.performerId == id)
                    .ToList())
            );
        }

        public async Task<ProjectInfoDTO> GetProjectInfo(int id)
        {
            return await Task.Run(() =>
            _mapper.Map<ProjectInfoDTO>(
                ProjectData
                    .SelectMany(p => p.Tasks, (p, t) => new { Project = p, Task = t })
                    .GroupBy(x => x.Project)
                    .Select(x => new ProjectInfo()
                    {
                        Project = x.Key,

                        LongestTaskByDescription = x.Select(x => new { task = x, descriptionLen = x.Task.description.Length })
                                                    .OrderByDescending(x => x.descriptionLen)
                                                    .Select(x => x.task.Task)
                                                    .FirstOrDefault(),

                        ShortestTaskByName = x.Select(x => new { task = x, nameLen = x.Task.name.Length })
                                              .OrderBy(x => x.nameLen)
                                              .Select(x => x.task.Task)
                                              .FirstOrDefault(),


                        CountUsersByCondition = x.Where(x => x.Project.description.Length > 20 || x.Project.Tasks.Count() < 3)
                                                    .Select(x => x.Project.Team.users.Count())
                                                    .FirstOrDefault()

                    }).FirstOrDefault(x => x.Project.id == id)
                    ));
        }
    }
}
