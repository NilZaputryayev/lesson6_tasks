﻿using Common.DTO.Team;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL
{
    public interface IApiTeamService
    {
        Task Delete(int id);
        Task<List<TeamDTO>> Get();
        Task<TeamDTO> GetByID(int id);
        Task Update(UpdateTeamDTO team);
        Task<TeamDTO> Create(NewTeamDTO user);

    }
}