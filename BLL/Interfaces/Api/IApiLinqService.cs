﻿using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL
{
    public interface IApiLinqService
    {
        public Task<Dictionary<ProjectDTO, int>> GetUserTaskCountDictionaryByID(int id);
        Task<List<TaskDTO>> ListTasksWith45NameByID(int id);
        Task<List<TaskIdName>> TaskListFinished2021ByID(int id);
        Task<List<TeamInfoDTO>> GetTeamsOlder10SortedAndGrouped();
        Task<Dictionary<string, List<TaskDTO>>> GetUserListSortedWithTasksAsync();
        Task<UserTasksInfoDTO> GetUserTasksInfo(int id);
        Task<ProjectInfoDTO> GetProjectInfo(int id);
        public Task<List<TaskDTO>> GetUserUnfinishedUserTasksByUserID(int id);


    }
}