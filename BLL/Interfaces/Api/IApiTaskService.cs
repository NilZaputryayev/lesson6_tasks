﻿using Common.DTO.Task;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL
{
    public interface IApiTaskService
    {
        Task Delete(int id);
        Task<List<TaskDTO>> Get();
        Task<TaskDTO> GetByID(int id);
        Task Update(UpdateTaskDTO task);
        Task<TaskDTO> Create(NewTaskDTO task);
    }
}