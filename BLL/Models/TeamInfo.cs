﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class TeamInfo
    {
        public int id { get; set; }
        public string name { get; set; }
        public IEnumerable<User> users { get; set; }
        
    }
}
