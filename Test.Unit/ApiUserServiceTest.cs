﻿using AutoMapper;
using BLL.MappingProfiles;
using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using Common.DTO.User;
using DAL;
using DAL.Context;
using DAL.Models;
using FakeItEasy;
using FakeItEasy.Sdk;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Xunit;

namespace BLL
{
    public class ApiUserServiceTest : IDisposable
    {
        readonly IApiUserService _apiUserService;

        public ApiUserServiceTest()
        {
            _apiUserService = A.Fake<IApiUserService>();
        }



        public void Dispose()
        {
        }

        [Fact]
        public void Create_WhenNewUserCreated_ThenReturnUserDTO()
        {
            var user = A.Fake<NewUserDTO>();
            var createdUser = A.Fake<UserDTO>();

            A.CallTo(() => _apiUserService.Create(user)).Returns(createdUser);

        }

        [Fact]
        public void Update_WhenUpdateUser_ThenUserUpdateCalled()
        {
            var user = A.Fake<UpdateUserDTO>();

            A.CallTo(() => _apiUserService.Update(user)).MustHaveHappened();

        }


    }


}
