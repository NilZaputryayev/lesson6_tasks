﻿using AutoMapper;
using DAL.Context;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BLL
{
    public class NewFuncApiLinqServiceTest : IClassFixture<DbFixture>, IDisposable
    {
        readonly ApiLinqService apiLinqService;
        private IMapper _mapper;
        private LinqEFContext _context;

        private DbFixture _dbFixture; 


        public NewFuncApiLinqServiceTest()
        {
            _dbFixture = new DbFixture();
            ConfigService();
            apiLinqService = new ApiLinqService(_context,_mapper);
        }

        private void ConfigService()
        {
            
            _mapper = _dbFixture.GetMapper();
            _context = _dbFixture.CreateContext();
        }

        public void Dispose()
        {
            _dbFixture.Dispose();
        }

       

        [Theory]
        [InlineData(35, 4)]
        [InlineData(28, 3)]
        public async Task GetUnfinishedUserTasks_WhenID_ThenReturnExpectedCountAsync(int id, int expectedCount)
        {

            var result = await apiLinqService.GetUserUnfinishedUserTasksByUserID(id);

            Assert.Equal(expectedCount, result.Count());


        }
       


    }
}
