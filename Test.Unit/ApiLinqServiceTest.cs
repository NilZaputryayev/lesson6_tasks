﻿using AutoMapper;
using DAL.Context;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BLL
{
    public class ApiLinqServiceTest : IClassFixture<DbFixture>, IDisposable
    {
        readonly ApiLinqService apiLinqService;
        private IMapper _mapper;
        private LinqEFContext _context;
        private DbFixture _dbFixture;


        public ApiLinqServiceTest()
        {
            _dbFixture = new DbFixture();
            ConfigService();

            apiLinqService = new ApiLinqService(_context, _mapper);
        }

        private void ConfigService()
        {
            _mapper = _dbFixture.GetMapper();
            _context = _dbFixture.CreateContext();
        }

        public void Dispose()
        {
            _dbFixture.Dispose();
        }

        [Fact]
        public async void GetUserTaskCountDictionaryByID_WhenIDEqual35_ThenReturn6()
        {
            int id = 35;
            int countUserTasks = 6;

            Assert.Equal(countUserTasks, (await apiLinqService.GetUserTaskCountDictionaryByID(id)).Count);
        }

        [Theory]
        [InlineData(62, 3)]
        [InlineData(45, 1)]
        [InlineData(28, 1)]
        [InlineData(20, 2)]
        [InlineData(66, 1)]
        public async void TaskListFinished2021ByID_WhenIDEqualID_ThenReturnExpectedCount(int id, int count)
        {

            Assert.Equal(count, (await apiLinqService.TaskListFinished2021ByID(id)).Count);

        }

        [Theory]
        [InlineData(20, 6)]
        [InlineData(21, 8)]
        [InlineData(22, 7)]
        public async void ListTasksWith45NameByID_WhenIDEqual_ThenReturnExpected(int id, int countTasks)
        {

            var tasks = apiLinqService.ListTasksWith45NameByID(id);

            Assert.Equal(countTasks, (await apiLinqService.ListTasksWith45NameByID(id)).Count);

        }

        [Fact]
        public async void ListTasksWith45NameByID_WhenIDEqual23_ThenLenOfEveryTaskNameLess45()
        {
            int id = 23;

            Assert.True((await apiLinqService.ListTasksWith45NameByID(id)).All(x => x.name.Length < 45));

        }


        [Fact]
        public async void GetUserListSortedWithTasks_WhenGetList_ThenMustBeenSortedByName()
        {
            var userList = (await apiLinqService.GetUserListSortedWithTasksAsync()).Keys.ToList();
            var userListSorted = userList.OrderBy(x => x).ToList();

            Assert.Equal(userList, userListSorted);
        }


        [Fact]
        public async System.Threading.Tasks.Task GetTeamsOlder10SortedAndGrouped_WhenGetList_ThenMustBeSortedAsync()
        {
            var data = (await apiLinqService.GetTeamsOlder10SortedAndGrouped()).ToList();
            var dataSorted = data.OrderBy(x => x.name).ToList();

            Assert.Equal(data, dataSorted);

        }

        [Fact]
        public async System.Threading.Tasks.Task GetTeamsOlder10SortedAndGrouped_WhenGetList_ThenAllUsersMustBeOlder10Async()
        {
            var allUsersOlder10 = (await apiLinqService.GetTeamsOlder10SortedAndGrouped()).All(x => x.users.All(y => DateTime.Now.Year - 10 > y.birthDay.Year));

            Assert.True(allUsersOlder10);

        }

        [Fact]
        public async System.Threading.Tasks.Task GetUserTasksInfo_WhenID28_ThenLastProjectNameIsWoodenAsync()
        {
            int id = 28;

            Assert.Equal("Wooden", (await apiLinqService.GetUserTasksInfo(id)).LastProject.name);
        }


        [Fact]
        public async System.Threading.Tasks.Task GetUserTasksInfo_WhenNoUser_ThenReturnNullAsync()
        {
            int id = 999;
            Assert.Null(await apiLinqService.GetUserTasksInfo(id));

        }


        [Fact]
        public async Task GetProjectInfo_WhenID0_ThenLastProjectNameIsIndexAsync()
        {

            Assert.Equal("index",(await apiLinqService.GetProjectInfo(0)).ShortestTaskByName.name);

        }





    }
}

