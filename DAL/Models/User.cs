﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace DAL.Models
{
    public class User
    {
        [Required]
        public int id { get; set; }
        public int teamId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }
        public DateTime registeredAt { get; set; }
        public DateTime birthDay { get; set; }
        public Team team { get; set; }
       
    }


}
